package com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Commands;

import com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Party;
import com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Reference;
import com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Util.ChatCreator;
import com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Util.Misc;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;


@SuppressWarnings({"ConstantConditions", "RedundantCast"})
public class party extends Command {
    public party() {
        super("party", "bpp.party.use", "p","par");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length <= 0){
            Sendhelp(sender);
        }else if(args[0].equalsIgnoreCase("help")){
            Sendhelp(sender);
        }else if(args[0].equalsIgnoreCase("create")){
            createParty(sender);
        }else if(args[0].equalsIgnoreCase("invite")){
            invitePlayer(sender,args);
        }else if(args[0].equalsIgnoreCase("accept")){
            acceptInvite(sender);
        }else if(args[0].equalsIgnoreCase("deny")){
            denyInvite(sender);
        }else if(args[0].equalsIgnoreCase("leave")){
            leaveParty(sender);
        }else if(args[0].equalsIgnoreCase("info")){
            SendInfo(sender);
        }else{
            Sendhelp(sender);
        }
    }

    private void SendInfo(CommandSender sender) {
        if (!Misc.checkIfInParty(sender)){sender.sendMessage(new ChatCreator(ChatColor.RED,"You aren't in a party").newLine().append(ChatColor.YELLOW,"You can create one with: ").append(ChatColor.GOLD,"/party create").create());return;}
        if(!Misc.checkIfPlayer(sender)){sender.sendMessage(new ChatCreator(ChatColor.RED, "This command can only be run by a player").newLine().append(ChatColor.WHITE, "You are:").append(ChatColor.GREEN, sender.getClass().getCanonicalName()).create());return;}
        Party party = Misc.getParty((ProxiedPlayer) sender);
        ChatCreator cc = new ChatCreator(ChatColor.GRAY, "==============Party Info==============").newLine()
                .append(ChatColor.DARK_BLUE, "PartyLeader:").append(ChatColor.YELLOW, party.getLeader().getDisplayName()).newLine()
                .append(ChatColor.DARK_BLUE, "PartyMembers");
        for(ProxiedPlayer curpl:party.getMembers()){
            cc.append(ChatColor.YELLOW,curpl.getDisplayName());
        }
        sender.sendMessage(cc.create());
    }

    private void leaveParty(CommandSender sender) {
        if(!Misc.checkIfPlayer(sender)){sender.sendMessage(new ChatCreator(ChatColor.RED, "This command can only be run by a player").newLine().append(ChatColor.WHITE, "You are:").append(ChatColor.GREEN, sender.getClass().getCanonicalName()).create());return;}
        if (!Misc.checkIfInParty(sender)){sender.sendMessage(new ChatCreator(ChatColor.RED,"You aren't in a party").create());return;}
            Party part = Misc.getParty((ProxiedPlayer) sender);
            part.removemember((ProxiedPlayer) sender);
            sender.sendMessage(new ChatCreator(ChatColor.GREEN,"You left ithe party").create());
    }

    private void denyInvite(CommandSender sender) {
        if(!Misc.checkIfPlayer(sender)){sender.sendMessage(new ChatCreator(ChatColor.RED, "This command can only be run by a player").newLine().append(ChatColor.WHITE, "You are:").append(ChatColor.GREEN, sender.getClass().getCanonicalName()).create());return;}
        if(Reference.invites.get((ProxiedPlayer) sender)==null){
            sender.sendMessage(new ChatCreator(ChatColor.RED,"You don't have any invites").create());
        }else{
            Party r = Reference.invites.remove((ProxiedPlayer) sender);
            sender.sendMessage(new ChatCreator(ChatColor.GREEN, "Invite from ").append(ChatColor.GOLD, r.getLeader().getDisplayName()).append(ChatColor.GREEN, " removed").create());
        }
    }

    private void acceptInvite(CommandSender sender) {
        if(!Misc.checkIfPlayer(sender)){sender.sendMessage(new ChatCreator(ChatColor.RED, "This command can only be run by a player").newLine().append(ChatColor.WHITE, "You are:").append(ChatColor.GREEN, sender.getClass().getCanonicalName()).create());return;}
        if(Reference.invites.get((ProxiedPlayer)sender)== null){
            sender.sendMessage(new ChatCreator(ChatColor.RED,"No invite found").create());
        }else{
            Party party = Reference.invites.get(sender);
            party.addmember((ProxiedPlayer) sender);
            sender.sendMessage(new ChatCreator(ChatColor.GREEN,"You have been added to ").append(ChatColor.GOLD,party.getLeader().getDisplayName()+"'s").append(ChatColor.GREEN," party").create());
            party.getLeader().sendMessage(new ChatCreator(ChatColor.GREEN, "Player ").append(ChatColor.GOLD, ((ProxiedPlayer) sender).getDisplayName()).append(ChatColor.GREEN, " has been added to your party").create());
        }
    }

    private void invitePlayer(CommandSender sender, String[] args) {
        if(!Misc.checkIfPlayer(sender)){sender.sendMessage(new ChatCreator(ChatColor.RED, "This command can only be run by a player").newLine().append(ChatColor.WHITE, "You are:").append(ChatColor.GREEN, sender.getClass().getCanonicalName()).create());return;}
        if(!Misc.checkIfInParty(sender)){sender.sendMessage(new ChatCreator(ChatColor.RED,"You need to be in a party to invite someone").newLine().append(ChatColor.YELLOW,"You can create one using: ").append(ChatColor.GOLD,"/party create").create());}
        if(!(args.length < 2)){
            ProxiedPlayer invited = ProxyServer.getInstance().getPlayer(args[1]);
            if (invited ==null){sender.sendMessage(new ChatCreator(ChatColor.RED,"Player:"+args[1]+" not Found").create());return;}
            if (Misc.checkIfInParty(invited)){sender.sendMessage(new ChatCreator(ChatColor.RED,"Player is already in a party").create());return;}
            Reference.invites.put(invited, Reference.parties.get(sender));
            invited.sendMessage(new ChatCreator(ChatColor.AQUA, "You were invited to a party by:" + ((ProxiedPlayer) sender).getDisplayName()).newLine().append(ChatColor.GOLD, "Type ").append(ChatColor.AQUA, "/party accept").italicise().append(ChatColor.GOLD, " to accept").create());
        }else{
            sender.sendMessage(new ChatCreator(ChatColor.RED, "Syntax: /party invite [playername]").create());
        }
    }

    private void createParty(CommandSender sender) {
        if(Misc.checkIfPlayer(sender)){
            if(Misc.checkIfInParty(sender)) {
                Reference.parties.put(sender, new Party((ProxiedPlayer) sender));
                sender.sendMessage(new ChatCreator(ChatColor.GREEN, "Party created").create());
            }else{
                sender.sendMessage(new ChatCreator(ChatColor.RED,"You are already in a party ").newLine().append(ChatColor.DARK_GREEN,"Please leave your current one and try again").create());
            }
        }else{
            sender.sendMessage(new ChatCreator(ChatColor.RED, "This command can only be run by a player").newLine().append(ChatColor.WHITE, "You are:").append(ChatColor.GREEN, sender.getClass().getCanonicalName()).create());
        }
    }

    private void Sendhelp(CommandSender sender){
       sender.sendMessage(new ChatCreator(ChatColor.DARK_BLUE, "PartyPlugin").append(ChatColor.WHITE, " help ")
               .newLine().append(ChatColor.GOLD, "/party create").newLine().append(ChatColor.WHITE, "creates a party")
               .newLine().append(ChatColor.GOLD, "/party invite [playername]").newLine().append(ChatColor.WHITE, "invites a player to the party")
               .newLine().append(ChatColor.GOLD, "/party accept").newLine().append(ChatColor.WHITE, "accepts the active invitation")
               .newLine().append(ChatColor.GOLD, "/party deny").newLine().append(ChatColor.WHITE, "denies the active invitation")
               .newLine().append(ChatColor.GOLD, "/party leave").newLine().append(ChatColor.WHITE, "leaves your current party")
               .newLine().append(ChatColor.GOLD, "/party info").newLine().append(ChatColor.WHITE, "Shows the info about your current party").create());
    }
}
