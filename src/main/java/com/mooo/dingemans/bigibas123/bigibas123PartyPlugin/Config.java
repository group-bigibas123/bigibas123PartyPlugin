package com.mooo.dingemans.bigibas123.bigibas123PartyPlugin;

import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;


@SuppressWarnings("ResultOfMethodCallIgnored")
public class Config {
    @Getter
    public final Configuration config;
    @Getter
    public final ConfigurationProvider provider;
    @Getter
    public final File file;
    public Config(Plugin plugin) {
        Configuration config1;
        provider = ConfigurationProvider.getProvider(YamlConfiguration.class);
        file = new File(plugin.getDataFolder(),"config.yml");
        file.mkdirs();
        try {
            config1 = provider.load(file);
        } catch (IOException e) {
            e.printStackTrace();
            config1 = new Configuration();
        }
        config = config1;
    }

}
