package com.mooo.dingemans.bigibas123.bigibas123PartyPlugin;


import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Listeners implements Listener {
    @EventHandler()
    public void onServerSwitch(ServerSwitchEvent event){
        Party p = Reference.parties.get(event.getPlayer());
        if(p!=null){
            p.connectall(event.getPlayer().getServer());
        }
    }
}
