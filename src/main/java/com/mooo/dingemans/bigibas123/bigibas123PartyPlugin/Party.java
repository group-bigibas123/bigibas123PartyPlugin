package com.mooo.dingemans.bigibas123.bigibas123PartyPlugin;


import com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Util.ChatCreator;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;

import java.util.ArrayList;

@SuppressWarnings("FinalizeCalledExplicitly")
public class Party {
    @Getter private final ProxiedPlayer leader;
    @Getter private final ArrayList<ProxiedPlayer> members;
    public Party(ProxiedPlayer leader){
        this.leader = leader;
        this.members = new ArrayList<>();
        this.members.add(leader);
    }
    public boolean addmember(ProxiedPlayer pl){
        if(this.members.contains(pl)){return false;}
        this.members.add(pl);
        return true;
    }
    public boolean removemember(ProxiedPlayer pl){
        if(pl == this.getLeader()){
            this.remove();
            return true;
        }else {
            return this.members.remove(pl);
        }
    }

    public boolean connectall(Server target){
        //efficiency no unnecesairy objects in the loop
        BaseComponent[] errormsg = new ChatCreator(ChatColor.RED, "You are already connected to ").append(ChatColor.GOLD,target.getInfo().getName()).append(ChatColor.RED," which party leader connected to").create();
        for(ProxiedPlayer pl:members){
            if(!pl.getServer().getInfo().getName().equalsIgnoreCase(target.getInfo().getName())){
                pl.connect(target.getInfo());
            }else{
                pl.sendMessage(errormsg);
            }
        }
        return true;
    }

    public void remove(){
        try {this.finalize();} catch (Throwable ignored) {}
    }
    @Override
    public void finalize() throws Throwable {
        if(Reference.parties.get(this.getLeader())!=null){
            Reference.parties.remove(this.getLeader());
        }
        int i = 0;
        do {
            members.remove(0);
        }while (i<members.size());
        super.finalize();
    }

}
