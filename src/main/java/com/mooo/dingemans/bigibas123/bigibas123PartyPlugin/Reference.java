package com.mooo.dingemans.bigibas123.bigibas123PartyPlugin;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.HashMap;


public class Reference {
    public static Plugin plugin;
    public static Config config;
    public static String pluginName;
    public static HashMap<CommandSender,Party> parties;
    public static HashMap<ProxiedPlayer, Party> invites;
}
