package com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Util;

import com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Reference;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;


@SuppressWarnings("unused")
public class ChatCreator {
    private final ComponentBuilder componentCreator;

    public ChatCreator(ChatColor maincolor,String text){
        this.componentCreator = new ComponentBuilder("[").color(ChatColor.DARK_GREEN).append(Reference.pluginName).color(ChatColor.AQUA).append("]").color(ChatColor.DARK_GREEN);
        this.componentCreator.append(text).color(maincolor);
    }
    public ChatCreator append(ChatColor color, String text){
        this.componentCreator.append(text).color(color);
        return this;
    }
    public ChatCreator append(String text){
        this.componentCreator.append(text);
        return this;
    }
    public ChatCreator newLine(){
        this.componentCreator.append("\n");
        return this;
    }
    public ChatCreator bold(){
        this.componentCreator.bold(true);
        return this;
    }
    public ChatCreator obfuscate(){
        this.componentCreator.obfuscated(true);
        return this;
    }
    public ChatCreator italicise(){
        this.componentCreator.italic(true);
        return this;
    }
    public ChatCreator strikethrough(){
        this.componentCreator.strikethrough(true);
        return this;
    }
    public ChatCreator underline(){
        this.componentCreator.underlined(true);
        return this;
    }
    public ChatCreator addClickEvent(ClickEvent event){
        this.componentCreator.event(event);
        return this;
    }
    public ChatCreator addHoverEvent(HoverEvent event){
        this.componentCreator.event(event);
        return this;
    }
    public BaseComponent[] create(){
        return this.componentCreator.create();
    }

}
