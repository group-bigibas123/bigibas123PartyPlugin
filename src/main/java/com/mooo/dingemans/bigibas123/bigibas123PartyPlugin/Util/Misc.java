package com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Util;

import com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Party;
import com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Reference;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;


@SuppressWarnings("SuspiciousMethodCalls")
public class Misc {
    //returns true if sender is a player
    public static boolean checkIfPlayer(CommandSender sender){
        return sender instanceof ProxiedPlayer;
    }
    public static boolean checkIfInParty(CommandSender sender){
        for (HashMap.Entry<CommandSender, Party> entry : Reference.parties.entrySet()) {
            if(entry.getValue().getMembers().contains(sender)){
                return true;
            }
        }
        return false;
    }
    public static Party getParty(ProxiedPlayer pl){
        for(HashMap.Entry<CommandSender,Party> entry:Reference.parties.entrySet()){
            if(entry.getValue().getMembers().contains(pl)){
                return entry.getValue();
            }
        }
        return null;
    }
}