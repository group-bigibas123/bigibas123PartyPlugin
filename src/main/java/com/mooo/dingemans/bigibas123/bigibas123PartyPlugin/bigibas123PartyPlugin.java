package com.mooo.dingemans.bigibas123.bigibas123PartyPlugin;


import com.mooo.dingemans.bigibas123.bigibas123PartyPlugin.Commands.party;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.HashMap;


@SuppressWarnings("unused")
public class bigibas123PartyPlugin extends Plugin {
    @Override
    public void onLoad(){
        Reference.plugin = this;
        Reference.pluginName = "PartyPlugin";
        Reference.config = new Config(this);
        Reference.parties = new HashMap<>();
        Reference.invites = new HashMap<>();
    }
    @Override
    public void onEnable(){
        this.getProxy().getPluginManager().registerCommand(this, new party());
        this.getProxy().getPluginManager().registerListener(this, new Listeners());
    }
}
